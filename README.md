# react-calendar

This is a fairly generic React component that can display a single month or a
whole calendar of months which is just a month and then some number of previous
months as well. There are probably a bunch of these, but I wanted some practice
working with React and I needed this specifically for another project. I would
strongly consider something else, like the
[react-calendar on npm](https://www.npmjs.com/package/react-calendar).

## Features

- Display one or more months in a single widget (not like traditional date
  pickers which require you to navigate to a given month and display one at a
  time)
- Easily style one or more dates with one or more custom classes. The
  `Calendar` component will automatically add a `selected` class to the provided day
  but `Month` requires doing this manually (thought most use cases can be
  satisfied by using a `Calendar` with 0 months of history).
- Attach a click handler which lets the components function as date selectors.
  The appropriate [luxon](https://moment.github.io/luxon/#/) `DateTime` is passed
  to the click handler.

## Demo

[Demo here](https://phinnaeus.gitlab.io/react-calendar/).

## Installing

As of now `@phinnaeus/react-calendar` is only hosted in the Gitlab NPM package
registry local to this project. To use it, you'll need to add the following to
your Yarn 2 `.yarnrc.yml` file:

```yaml
npmScopes:
  phinnaeus:
    npmRegistryServer: "https://gitlab.com/api/v4/projects/29391616/packages/npm/"
```

Then just run `yarn add @phinnaeus/react-calendar`.

There's a similar configuration for bare NPM's `.npmrc` but I don't know it.

## Usage

For a set of complete examples, see `example/index.tsx`. In general, you can
pass in dates as either luxon `DateTime` instances or ISO 8601 strings.

### Calendar

```tsx
import { DateTime } from "luxon";
import { Calendar } from "@phinnaeus/react-calendar";

<Calendar
  date={DateTime.now()}
  firstWeekDay={1} // Weeks should start on Monday instead of Sunday (default/0)
  historyMonths={5} // Show 6 total months
/>;
```

### Calendar (styled)

```tsx
import { Calendar } from "@phinnaeus/react-calendar";
import { DateTime } from "luxon";

const dateClasses = (date: DateTime) => {
  if (date == "2021-04-01") {
    return ["april-fools"];
  } else if (date == "2021-03-17") {
    return ["st-patricks-day"];
  }
  return [];
};

<Calendar
  date={"2021-05-12"}
  historyMonths={2} // Show 3 total months
  dateClasses={dateClasses}
/>;
```

### Single Month

```tsx
import { DateTime } from "luxon";
import { Month } from "@phinnaeus/react-calendar";

<Month date={DateTime.now()} />;
```

### Single Month (styled)

```tsx
import {Month} from "@phinnaeus/react-calendar";

// note that month only takes the day ordinal for styling
const dayClasses = {
    10: ['mario-day'],
    14: ['pi-day', 'napping-day'],
    17: ['st-patricks-day'],
}

<Month date={'2021-03-01'} dayClasses={dayClasses} />
```

# `PerDateMetadata`

This is a convenience class if you want to specify your `dateClasses` as an object
rather than a function. It can be passed into a `Calendar` or `Month`, but make sure
to bind the `this` property.

It's API is compatible with `@luxon` types as well as standard `YYYY-MM-DD` string dates.

```tsx
import { Month, PerDateMetadata } from "@phinnaeus/react-calendar";

const md = new PerDateMetadata();
md.add("2021-07-04", "usa-birthday");

<Month date={"2021-07-01"} dateClasses={md.bind(md)} />;
```

# Development

Development is pretty simple. Make sure you have Yarn installed. You should be
able to just run `yarn install` and then `yarn dev` when you want to launch the
examples in a local web server. See `package.json` for all commands.

# Credits

- Repository icon made by [Freepik](https://www.freepik.com) from [www.flaticon.com](https://www.flaticon.com)
