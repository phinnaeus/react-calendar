import React from "react";
import { createRoot } from "react-dom/client";
import { DateTime } from "luxon";

import { Calendar } from "./Calendar";

test("calendar view renders", () => {
  const root = createRoot(document.createElement("div"));
  root.render(<Calendar date={DateTime.fromISO("2021-06-24")} />);
});
