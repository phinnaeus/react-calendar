import React from "react";
import { createRoot } from "react-dom/client";
import { DateTime } from "luxon";

import { Month, monthDays, shiftElements } from "./Month";

test("month view renders", () => {
  const root = createRoot(document.createElement("div"));
  root.render(<Month date={DateTime.fromISO("2021-06-24")} />);
});

test("shift elements", () => {
  expect(shiftElements(["a", "b", "c"])).toEqual(["a", "b", "c"]);
  expect(shiftElements(["a", "b", "c"], 0)).toEqual(["a", "b", "c"]);
  expect(shiftElements(["a", "b", "c"], 1)).toEqual(["b", "c", "a"]);
  expect(shiftElements([1, 2, 3, 4], 3)).toEqual([4, 1, 2, 3]);
  expect(() => shiftElements(["a", "b", "c"], -1)).toThrow();
  expect(() => shiftElements(["a", "b", "c"], 8)).toThrow();
});

test("jan has 31 days (start sunday)", () => {
  const expected = [
    [0, 0, 0, 0, 0, 1, 2],
    [3, 4, 5, 6, 7, 8, 9],
    [10, 11, 12, 13, 14, 15, 16],
    [17, 18, 19, 20, 21, 22, 23],
    [24, 25, 26, 27, 28, 29, 30],
    [31, 0, 0, 0, 0, 0, 0],
  ];
  const month = monthDays(DateTime.fromISO("2021-01-01"));
  expect(month).toEqual(expected);
});

test("jan has 31 days (start monday)", () => {
  const expected = [
    [0, 0, 0, 0, 1, 2, 3],
    [4, 5, 6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15, 16, 17],
    [18, 19, 20, 21, 22, 23, 24],
    [25, 26, 27, 28, 29, 30, 31],
  ];
  const month = monthDays(DateTime.fromISO("2021-01-01"), 1);
  expect(month).toEqual(expected);
});

test("feb 2021 is a perfect month (for monday start)", () => {
  const expected = [
    [1, 2, 3, 4, 5, 6, 7],
    [8, 9, 10, 11, 12, 13, 14],
    [15, 16, 17, 18, 19, 20, 21],
    [22, 23, 24, 25, 26, 27, 28],
  ];
  const month = monthDays(DateTime.fromISO("2021-02-01"), 1);
  expect(month).toEqual(expected);
});

test("2020 is a leap year", () => {
  const expected = [
    [0, 0, 0, 0, 0, 0, 1],
    [2, 3, 4, 5, 6, 7, 8],
    [9, 10, 11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20, 21, 22],
    [23, 24, 25, 26, 27, 28, 29],
  ];
  const month = monthDays(DateTime.fromISO("2020-02-01"));
  expect(month).toEqual(expected);
});

test("do not need to start on the first day", () => {
  const expected = [
    [0, 0, 0, 0, 0, 0, 1],
    [2, 3, 4, 5, 6, 7, 8],
    [9, 10, 11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20, 21, 22],
    [23, 24, 25, 26, 27, 28, 29],
  ];
  const month = monthDays(DateTime.fromISO("2020-02-20"));
  expect(month).toEqual(expected);
});

test("august 2021 starts on a sunday (but we want monday)", () => {
  const expected = [
    [0, 0, 0, 0, 0, 0, 1],
    [2, 3, 4, 5, 6, 7, 8],
    [9, 10, 11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20, 21, 22],
    [23, 24, 25, 26, 27, 28, 29],
    [30, 31, 0, 0, 0, 0, 0],
  ];
  const month = monthDays(DateTime.fromISO("2021-08-01"), 1);
  expect(month).toEqual(expected);
});
