import React, { Component, ReactNode } from "react";

import { Month, MonthProps } from "./Month";

type CalendarProps = MonthProps & {
  historyMonths: number;
};

type CalendarState = Record<never, never>;

export class Calendar extends Component<CalendarProps, CalendarState> {
  static defaultProps = Object.assign(
    {
      historyMonths: 5,
    },
    Month.defaultProps
  );

  constructor(props: CalendarProps) {
    super(props);
    if (props.historyMonths < 0) {
      throw new RangeError("historyMonths should be >= 0");
    }
  }

  render(): ReactNode {
    const months = [];
    for (let i = this.props.historyMonths; i >= 0; i--) {
      const dt = this.props.date.minus({ months: i });

      months.push(
        <div
          className={this.props.customClasses.get("month-container")}
          key={dt.toFormat("yyyy-LL")}
        >
          <span>{dt.toFormat("LLLL yyyy")}</span>
          <Month {...this.props} date={dt} />
        </div>
      );
    }

    return (
      <div className={this.props.customClasses.get("calendar-container")}>
        {months}
      </div>
    );
  }
}
