import React, { Component, ReactNode } from "react";

import { DateTime } from "luxon";

const DEFAULT_WEEKDAYS: Array<string> = ["Su", "M", "Tu", "W", "Th", "F", "Sa"];
const DEFAULT_CLASSES: Array<[string, string]> = [
  ["calendar-container", "rc-calendar-container"],
  ["month-container", "rc-month-container"],
  ["month", "rc-month"],
  ["month-header", "rc-month-header"],
  ["month-days", "rc-month-days"],
  ["day", "rc-day"],
  ["selected-day", "rc-selected"],
];

export type MonthProps = {
  date: DateTime;
  selectedDate?: DateTime;
  firstWeekDay: number;
  dateClasses: (date: DateTime) => string[];
  onDateSelect: (date: DateTime) => void;
  customClasses: Map<string, string>;
  customWeekdayNames: Array<string>;
};

type MonthState = Record<string, never>;

export class Month extends Component<MonthProps, MonthState> {
  static defaultProps = {
    firstWeekDay: 0,
    dateClasses: () => [],
    onDateSelect: (): void => {
      // do nothing
    },
    customWeekdayNames: DEFAULT_WEEKDAYS,
    customClasses: new Map<string, string>(DEFAULT_CLASSES),
  };
  constructor(props: MonthProps) {
    super(props);
    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(day: number): void {
    if (day === 0) {
      return;
    }
    this.props.onDateSelect(this.props.date.set({ day: day }));
  }

  // TODO override shouldComponentUpdate to prevent unneeded updates

  render(): ReactNode {
    let selectedDay = -1;
    if (this.props.selectedDate) {
      if (this.props.date.hasSame(this.props.selectedDate, "month")) {
        selectedDay = this.props.selectedDate.day;
      }
    }

    const month = monthDays(this.props.date, this.props.firstWeekDay);
    const weekdays = shiftElements(
      this.props.customWeekdayNames,
      this.props.firstWeekDay
    );
    const headerCells = weekdays.map((wd) => <th key={wd}>{wd}</th>);
    const weekRows = month.map((week, i) => {
      const dayCells = week.map((day, i) => {
        let classNames: Array<string | undefined> = [];
        if (day > 0) {
          classNames.push(this.props.customClasses.get("day"));
          const today = this.props.date.set({ day: day });
          classNames = classNames.concat(this.props.dateClasses(today));
          if (day == selectedDay) {
            classNames.push(this.props.customClasses.get("selected-day"));
          }
        }
        return (
          <td
            key={i}
            className={classNames.join(" ")}
            onClick={() => this.handleClick(day)}
          >
            {day === 0 ? "" : day.toString()}
          </td>
        );
      });

      return <tr key={i}>{dayCells}</tr>;
    });
    return (
      <table className={this.props.customClasses.get("month")}>
        <thead className={this.props.customClasses.get("month-header")}>
          <tr>{headerCells}</tr>
        </thead>
        <tbody className={this.props.customClasses.get("month-days")}>
          {weekRows}
        </tbody>
      </table>
    );
  }
}

/**
 * TODO exported only for testing
 * @param date a lunxon DateTime which indicates what month to render
 * @param firstWeekDay an ordinal number for what day weeks should start on. 0 for Sunday, 1 for Monday, etc
 */
export function monthDays(
  date: DateTime,
  firstWeekDay = 0
): Array<Array<number>> {
  // a Date representing the first day of the month. normalize it if users are rude
  const firstDate = date.set({ day: 1 });

  // an ordinal number representation of what day of the week our month starts on
  // it is normalized since JS Date always has Sunday as the first day of the week
  const normalizedFirstDay = firstDate.weekday - firstWeekDay;
  const firstWeekdayOfMonth =
    normalizedFirstDay >= 0 ? normalizedFirstDay : 7 + normalizedFirstDay;

  const month = [];
  let week = [];
  for (let i = 1 - firstWeekdayOfMonth; i <= firstDate.daysInMonth; i++) {
    if (i <= 0) {
      week.push(0);
      continue;
    }
    if (week.length > 0 && week.length % 7 === 0) {
      month.push(week);
      week = [];
    }
    week.push(i);
  }
  month.push(padArray(week, 7, 0));
  return month;
}

function padArray<T>(array: Array<T>, length: number, fill: T): Array<T> {
  if (length > array.length) {
    return array.concat(Array(length - array.length).fill(fill));
  }
  return array;
}

// TODO exported only for testing
export function shiftElements<T>(array: Array<T>, n = 0): Array<T> {
  if (n >= array.length || n < 0) {
    throw Error("n should be within the size of the array");
  }
  return array.slice(n).concat(array.slice(0, n));
}
