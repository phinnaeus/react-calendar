import { DateTime } from "luxon";

export class PerDateMetadata {
  private readonly _map: Map<string, Array<string>>;
  private readonly _allowSpaces: boolean;
  constructor(
    entries?: (readonly [string | DateTime, Array<string>])[] | null,
    allowSpaces = false
  ) {
    this._map = new Map<string, Array<string>>();
    this._allowSpaces = allowSpaces;
    if (entries) {
      entries.forEach(([key, value]) => {
        if (typeof key === "string") {
          key = DateTime.fromISO(key);
        }
        this.addAll(key, value);
      });
    }
  }

  public add(date: DateTime | string, data: string): void {
    this.addAll(date, [data]);
  }

  public addAll(date: DateTime | string, data: Array<string>): void {
    if (!this._allowSpaces && data.some((s) => s.includes(" "))) {
      throw new Error("spaces in element values are not allowed");
    }

    if (typeof date !== "string") {
      date = date.toISODate();
    }

    const existing = this._map.get(date) || [];
    this._map.set(date, existing.concat(data));
  }

  public get(date: DateTime | string): Array<string> {
    if (typeof date !== "string") {
      date = date.toISODate();
    }
    // TODO should we return undefined instead?
    return this._map.get(date) || [];
  }

  public has(date: DateTime | string): boolean {
    if (typeof date !== "string") {
      date = date.toISODate();
    }
    return this._map.has(date);
  }

  public with(
    entries: (readonly [string | DateTime, Array<string>])[]
  ): PerDateMetadata {
    const r = new PerDateMetadata(null, this._allowSpaces);
    // TODO more efficient way to do this
    this._map.forEach((v, k) => {
      r.addAll(k, v);
    });

    // TODO more efficient way to do this
    entries.forEach(([key, value]) => {
      if (typeof key === "string") {
        key = DateTime.fromISO(key);
      }
      r.addAll(key, value);
    });
    return r;
  }

  public size(): number {
    return this._map.size;
  }
}
