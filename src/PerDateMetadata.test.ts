import { PerDateMetadata } from "./PerDateMetadata";
import { DateTime } from "luxon";

test("basic operation", () => {
  const md = new PerDateMetadata();
  const date = DateTime.fromISO("2021-07-04");
  md.add(date, "usa-birthday");
  md.addAll(date, ["more", "stuff"]);
  expect(md.get(date)).toEqual(["usa-birthday", "more", "stuff"]);
  expect(md.has(date)).toBeTruthy();
  expect(md.has(DateTime.fromISO("2021-07-05"))).toBeFalsy();
});

test("basic operation (strings)", () => {
  const md = new PerDateMetadata();
  md.add("2021-07-04", "usa-birthday");
  md.addAll("2021-07-04", ["more", "stuff"]);
  expect(md.get("2021-07-04")).toEqual(["usa-birthday", "more", "stuff"]);
  expect(md.has("2021-07-04")).toBeTruthy();
  expect(md.has("2021-07-05")).toBeFalsy();
});

test("string map constructor", () => {
  const md = new PerDateMetadata(
    Object.entries({
      "2021-07-01": ["usa-birthday"],
      "2021-03-17": ["st-patricks-day"],
    })
  );
  expect(md.size()).toEqual(2);
});

test("DateTime map constructor", () => {
  const md = new PerDateMetadata([
    [DateTime.fromISO("2021-07-01"), ["usa-birthday"]],
    [DateTime.fromISO("2021-03-17"), ["st-patrick-day"]],
  ]);
  expect(md.size()).toEqual(2);
});

test("spaces are allowed", () => {
  const md = new PerDateMetadata([], true);
  md.add(DateTime.now(), "this has spaces");
});

test("spaces are not allowed", () => {
  const md = new PerDateMetadata([], false);
  expect(() => {
    md.add(DateTime.now(), "this has spaces");
  }).toThrow();
});

test("with() functionality", () => {
  const md = new PerDateMetadata();
  md.add("2021-07-04", "usa-birthday");
  expect(md.size()).toEqual(1);

  const newMd = md.with(
    Object.entries({
      "2021-07-04": ["usa-birthday-2"],
      "2021-03-17": ["st-patricks-day"],
    })
  );
  expect(md.size()).toEqual(1);
  expect(newMd.size()).toEqual(2);
  expect(newMd.get("2021-07-04").length).toEqual(2);
});
