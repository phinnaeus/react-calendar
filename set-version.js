const semver = require("semver");
const fs = require("fs");

const versionStr = process.argv[2];
if (!versionStr) {
  console.error("version is required");
  process.exit(1);
}

if (!semver.valid(versionStr)) {
  console.error("semver is invalid");
  process.exit(1);
}

const version = semver.parse(versionStr);
console.log('Setting version "%s"', version);

const data = fs.readFileSync("package.json", "utf8");
const parsed = JSON.parse(data);

if (parsed.hasOwnProperty("version")) {
  console.error("package.json already has a version");
  process.exit(1);
}

parsed["version"] = version.toString();

fs.writeFileSync("package.json", JSON.stringify(parsed, null, 2), "utf8");

console.log("done!");
