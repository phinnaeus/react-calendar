import React from "react";
import { createRoot } from "react-dom/client";
import { DateTime } from "luxon";

import { Calendar, Month, PerDateMetadata } from "../src";

const today = DateTime.fromISO("2021-10-03");

const dateClasses = new PerDateMetadata();
dateClasses.add(DateTime.fromISO("2021-05-21"), "tyler");
dateClasses.add(DateTime.fromISO("2021-03-19"), "john");
dateClasses.add(DateTime.fromISO("2021-09-25"), "robin");
dateClasses.add(today, "today");

const dayClasses = new Map<number, Array<string>>();
dayClasses.set(10, ["robin"]);
dayClasses.set(11, ["john"]);
dayClasses.set(13, ["tyler"]);

type StatefulCalendarProps = {
  initialDate: DateTime;
  dateClasses: (date: DateTime) => string[];
};

type StatefulCalendarState = {
  selectedDate: DateTime;
};

class StatefulCalendar extends React.Component<
  StatefulCalendarProps,
  StatefulCalendarState
> {
  constructor(props: StatefulCalendarProps) {
    super(props);
    this.state = {
      selectedDate: props.initialDate,
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date: DateTime) {
    this.setState({
      selectedDate: date,
    });
  }

  render() {
    return (
      <>
        <span className={"selected-date"}>
          Selected Date: {this.state.selectedDate.toLocaleString()}
        </span>
        <Calendar
          date={this.props.initialDate}
          selectedDate={this.state.selectedDate}
          firstWeekDay={1}
          onDateSelect={this.onDateChange}
          historyMonths={2}
          dateClasses={this.props.dateClasses}
          customWeekdayNames={["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]}
        />
      </>
    );
  }
}

const container = document.querySelector("#react-container");
if (!container) {
  throw new Error("Unable to find #react-container in DOM");
}
const root = createRoot(container);
root.render(
  <>
    <h2>Calendar Example</h2>
    <Calendar
      date={today}
      firstWeekDay={1}
      historyMonths={7}
      dateClasses={dateClasses.get.bind(dateClasses)}
    />
    <h2>Standalone Month Example</h2>
    <Month date={today} />
    <h2>Custom Stateful Calendar Example (click a date)</h2>
    <StatefulCalendar
      initialDate={today}
      dateClasses={dateClasses.get.bind(dateClasses)}
    />
  </>
);
